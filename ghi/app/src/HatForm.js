import React, { useState, useEffect } from "react";

function HatForm() {
  const [locations, setLocations] = useState([]);
  const [formData, setFormData] = useState({
    fabric: "",
    style_name: "",
    color: "",
    pic_url: "",
    location: "",
  });

  const getData = async () => {
    const url = "http://localhost:8090/api/hats/new";
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setLocations(data.locations);
    }
  };

  useEffect(() => {
    getData().then(()=>{console.log(locations)})
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    console.log(formData)
    const newFormData = {
        ...formData,
        location: parseInt(formData.location)
    }


    const locationUrl = "http://localhost:8090/api/hats/";

    const fetchConfig = {
        method: "POST",
        body: JSON.stringify(newFormData),
        headers: {
          'Content-Type': 'application/json',
        },
    };

        const response = await fetch(locationUrl, fetchConfig);

        if (response.ok) {
            setFormData({
                fabric: "",
                style_name: "",
                color: "",
                pic_url: "",
                location: "",
            });
          }
        }

        const handleFormChange = (e) => {
          const value = e.target.value;
          const inputName = e.target.name;
          setFormData({
            ...formData,

            [inputName]: value
          });
    }

    return (
        <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a new hat</h1>
            <form onSubmit={handleSubmit} id="create-hat-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.fabric} placeholder="Fabric" required type="text" name="fabric" id="fabirc" className="form-control" />
                <label htmlFor="fabric">Fabric</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.style_name} placeholder="style_name" required type="text" name="style_name" id="style_name" className="form-control" />
                <label htmlFor="style name">Style Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.color} placeholder="color" required type="text" name="color" id="color" className="form-control" />
                <label htmlFor="color">Color</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.pic_url} placeholder="pic_url" required type="url" name="pic_url" id="pic_url" className="form-control" />
                <label htmlFor="pic_url">Pic URL</label>
              </div>
              <div className="mb-3">
              <input onChange={handleFormChange} value={formData.location} placeholder="location" required type="text" name="location" id="location" className="form-control" />
                <label htmlFor="location">location</label>
                {/* <select onChange={handleFormChange} value={formData.location} required type="in" id="location" className="form-select">
                  <option value="">Choose a location</option>
                  {locations.map(location => {
                    return (
                      <option key={location.href} value={location.id}>{location.name}</option>
                    )
                  })}
                </select> */}
              </div>
              <button className="btn btn-primary">Create</button>
            </form>
          </div>
        </div>
      </div>
    );
}
  export default HatForm;
