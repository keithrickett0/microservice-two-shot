import { useEffect, useState } from 'react';

function HatList() {
  const [hats, setHats] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8090/api/hats/list');

    if (response.ok) {
      const data = await response.json();
      console.log("here is a message")
      setHats(data.hats)
    }
  }

  useEffect(()=>{
    getData()
  }, [])

  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>fabric</th>
          <th>style name</th>
          <th>color</th>
          <th>pic</th>
          <th>location</th>
        </tr>
      </thead>
      <tbody>
        {hats.map(hat => {
          return (
            <tr key={hat.href}>
              <td>{ hat.fabric }</td>
              <td>{ hat.style_name }</td>
              <td>{ hat.color }</td>
              <td>{ hat.pic_url }</td>
              <td>{ hat.location }</td>
            </tr>
          );
        })}
      </tbody>
    </table>
  );
}

export default HatList;
