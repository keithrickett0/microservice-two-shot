import React, { useState } from 'react'


const shoeData = {
  manufacturer: "",
  modelName: "",
  color: "",
  binNumber: ""
}


const ShoeForm = (props) => {
  const [shoe, setShoe] = useState(shoeData)
  return (
    <div>
      <form>
        <label>Manufacturer</label>
        <input
          type="text"
          name="manufacturer"
          value={shoe.manufacturer}
          onChange={(e) => setShoe({
            ...shoe,
            manufacturer: e.target.value
          })}
        />
        {/* <input
          type="text"
          name="model_name"
          value={shoe.manufacturer}
          onChange={(e) => setShoe({
            ...shoe,
            manufacturer: e.target.value
          })}
        />
        <input
          type="text"
          name="manufacturer"
          value={shoe.manufacturer}
          onChange={(e) => setShoe({
            ...shoe,
            manufacturer: e.target.value
          })}
        />
        <input
          type="text"
          name="manufacturer"
          value={shoe.manufacturer}
          onChange={(e) => setShoe({
            ...shoe,
            manufacturer: e.target.value
          })}
        /> */}
      </form>
    </div>
  )
}
