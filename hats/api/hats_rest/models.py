from django.db import models
from django.urls import reverse

# Create your models here.


class LocationVO(models.Model):
    import_href = models.CharField(max_length=200)

    def __str__(self):
        return f"The path is {self.import_href}"


class Hat(models.Model):
    fabric = models.CharField(max_length=200)
    style_name = models.CharField(max_length=200)
    color = models.CharField(max_length=200)
    pic_url = models.URLField()
    location = models.ForeignKey(LocationVO, on_delete=models.CASCADE)

    def get_api_url(self):
        return reverse("api_hat", kwargs={"pk": self.pk})

    def __str__(self):
        return f"{self.fabric} - {self.style_name}/{self.color}/{self.pic_url}/{self.location}"
