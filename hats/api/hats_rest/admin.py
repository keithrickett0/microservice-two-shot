from django.contrib import admin
from .models import Hat, LocationVO

admin.site.register(Hat)
admin.site.register(LocationVO)
