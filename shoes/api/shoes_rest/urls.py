from django.urls import path
from .views import get_shoes, create_shoe, update_shoe, delete_shoe

urlpatterns = [
    path("all/", get_shoes, name="get_shoes"),
    path("", create_shoe, name="create_shoe"),
    path("update/<int:pk>/", update_shoe, name="update_shoe"),
    path("delete/<int:pk>/", delete_shoe, name="delete_shoe"),
]
