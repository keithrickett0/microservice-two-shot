from django.shortcuts import render
from django.http import JsonResponse
from .models import Shoe, BinVO
# from wardrobe_api.models import Bin
from common.json import ModelEncoder
from django.core import serializers

import json

class BinListEncoder(ModelEncoder):
    model = BinVO
    properties = [
        "closet_name",
        "bin_number",
        "bin_size"
    ]


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = [
        "manufacturer",
        "model_name",
        "color",
        "url",
        "bin_id",
    ]
    encoders = {
        "bin_id": BinListEncoder
    }


def get_shoes(request):
    shoes = Shoe.objects.all()
    shoe_data = [{'id': shoe.id, 'manufacturer': shoe.manufacturer, 'color': shoe.color, 'model_name': shoe.model_name, 'bin_id': shoe.bin_id, 'url': shoe.url} for shoe in shoes]
    return JsonResponse(
        {"shoes": shoe_data},
        safe=False
    )


def create_shoe(request):
    try:
        content = json.loads(request.body)
        new_shoe = Shoe.objects.create(**content)
        return JsonResponse(
            content,
            encoder=ShoeDetailEncoder
        )
    except Exception as e:
            return JsonResponse({'error': str(e)}, status=400)


def update_shoe(request, pk):
    try:
        content = json.loads(request.body)
        Shoe.objects.filter(id=pk).update(**content)
        return JsonResponse(content)
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)


def delete_shoe(request, pk):
    try:
        Shoe.objects.filter(id=pk).delete()
        return JsonResponse({"message": "Successfully Deleted"})
    except Exception as e:
        return JsonResponse({'error': str(e)}, status=400)
